/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#ifndef ROSPARAM_UTIL_H_ECAQTGUZ
#define ROSPARAM_UTIL_H_ECAQTGUZ

#include <vector>
#include <string>
#include <algorithm>

#include <Eigen/Core>

#include <ros/ros.h>

namespace ca
{
namespace rosparam_util
{

template<class Scalar, int N>
bool GetVectorParam(const ros::NodeHandle& nh,
                    const std::string& key,
                    Eigen::Matrix<Scalar, N, 1> & vec) {

  std::vector<Scalar> tmpvec;
  if (!nh.getParam(key, tmpvec)) { return false; }
  if (tmpvec.size() != N) {
    ROS_ERROR_STREAM("parameter " << key << " is size " << tmpvec.size()
                     << ", expected " << N);
    return false;
  }
  std::copy(tmpvec.begin(), tmpvec.end(), vec.data());
  //vec = Eigen::Map<Eigen::Matrix<Scalar, N, 1> >(tmpvec.data());
  return true;
}


/**
 * \brief Wrapper for handling access of ros parameters.
 *
 * Will call error stream if unable to retrieve ros param.
 * @param n the ros node handle
 * @param param_name the name of the parameter
 * @param param (output) where the parameter will be save to if found
 * @return True if parameter found.  False otherwise
 */
template<typename T>
bool GetParam(ros::NodeHandle &n, const std::string param_name, T& param) {
  if (!n.getParam(param_name, param)) {
    ROS_ERROR_STREAM(
        ros::this_node::getName()<<": Could not load parameter: "<<param_name);
    return false;
  } else {
    return true;
  }
}

} /* rosparam_util */
} /* ca */

#endif /* end of include guard: ROSPARAM_UTIL_H_ECAQTGUZ */

