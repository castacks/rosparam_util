/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/



#include <ros/console.h>
#include <rosparam_util/rosparam_util.h>

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "foonode");

  ros::NodeHandle nh("~");
  Eigen::Vector3d v1;
  bool found = ca::rosparam_util::GetVectorParam<double, 3>(nh, "foo", v1);
  if (found) {
    ROS_INFO_STREAM("v1 = " << v1.transpose());
  } else {
    ROS_INFO_STREAM("foo not found");
  }
  return 0;
}
